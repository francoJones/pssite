import {InScroll} from "@libs/InScroll";
import {isEdge} from "@consts/Browser";
import LetterFX from "@libs/LetterFX";
import {LAZYFX_LOADING} from '@consts/ClassNames';


const ITEM: string = 'content__item';

/** TODO all the lazy & lazy-loaded related elements could be stored into an object,
 * so there is no need to climb up the DOM tree in lazyLoadUpdated every time Case is entered.
 */

export default class CaseFX {
    root: HTMLElement;
    selector: string;
    nodeList: NodeListOf<HTMLElement>;
    node: HTMLElement = {} as HTMLElement;

    constructor(selector: string) {
        this.root = document.querySelector(selector) as HTMLElement;
        this.selector = selector;
        this.nodeList = this.root.querySelectorAll(`.${ITEM} > div > div`);
    }

    buildIntro = (): void => {

        let letterFX: LetterFX = new LetterFX(['.title__job', '.title__role']);
        setTimeout(() => letterFX.runIntro());

        // content is hidden, so it shows up as scrolling + lazyLoad happen
        for (let node of this.nodeList) {
            Object.assign(node.style, {
                opacity: 0,
                transform: `translate3d(0, ${50 + 100 * Math.random()}px, 0)`,
                transition: `${Math.random() / 2 + .15}s ease-out ${Math.random() / 2}s`
            })
        }
    }


    lazyLoadUpdated(img: HTMLElement): void {
        let parent: HTMLElement | null = img;
        while (!parent.classList.contains(ITEM)) {
            if (parent.classList.contains(LAZYFX_LOADING))
                parent.classList.remove(LAZYFX_LOADING);
            parent = parent.parentElement;
            if (!parent) break;
        }
        if (!parent) return;

        parent.classList.remove('lazyFX-loading');// TODO un-hardcode this
        let children: NodeListOf<HTMLElement> = isEdge ?
            parent.parentElement!.querySelectorAll(` div > div`) :
            parent.querySelectorAll(`:scope > div > div`);

        for (let node of children) {
            Object.assign(node.style, {
                opacity: 1,
                transform: `translate3d(0, 0, 0)`
            })
        }
        InScroll.update();
    }
}
