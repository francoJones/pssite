export interface IGallery {
    current: number
}

export interface ISections {
    current: string;
    dat: { [k: string]: any };
}

export interface IActionVal {
    prop: string;
    val: any
}

export type model = {
    gallery: IGallery,
    sections: ISections
}

export interface IImg{
    isZoomed: boolean,
    index?: number,
    src: string,
    onClick?: (p: number) => void,
    type: string | null,
    className?: string,
    showText:boolean,
    internal_link?:string
}




