import {TIME_3 as TIME} from '@consts/Transforms';
import * as css from '../../scss/export.scss'; // TODO how can this be aliased? tsconfig fails


function sToNum(n: string): number {
    return Number(TIME.replace('s', '')) * 1000
}

export const CoverFX = (function () {

    // cover is the invisible container that takes the entire screen and is position: fixed
    // patch is the element changing in size and covering everything. Once the entire screen is covered,
    // the content underneath changes and then patch is rescaled to 0 and then cover is hidden
    let cover: HTMLElement;
    let patch: HTMLElement;
    return {
        buildIntro: (): void => {

            cover = document.createElement('div');
            patch = document.createElement('div');
            Object.assign(cover.style, {
                position: 'fixed',
                top: 0,
                right: 0,
                width: '100vw',
                height: '100vh',
                zIndex: 9999,
                overflow: 'hidden'
            });

            // top to bottom patch
            Object.assign(patch.style, {
                position: 'absolute',
                top: 0,
                right: 0,
                width: '100%',
                height: '100%',
                transform: 'scaleY(0)',
                background: css.beige40,
                zIndex: 10
            });

            cover.appendChild(patch);

            document.querySelector('body')!.appendChild(cover);

        }

        , runIntro: (t?: number, cb?: (p: any) => void, args?: string): void => {

            let time: number = t || sToNum(TIME);
            let delay: number = 0;

            setTimeout(() => {
                Object.assign(patch.style, {
                    transformOrigin: 'right 0',
                    transform: 'scaleY(1)',
                    transition: `transform ${TIME} ease-in-out`,
                })


            }, delay += time);

            // The first timeout covers the screen; this if(cb)timeout loads the content under the patch,
            // the next timeout removes the patch
            if (cb) setTimeout(() => {
                cb(args)
            }, delay + time);

            setTimeout(() => {

                Object.assign(patch.style, {
                    transform: 'scaleX(0)'
                })
            }, delay += time);

            setTimeout(() => {
                cover.style.visibility = 'hidden'
            }, delay + time);

        }

        , runOutro: (t?: number, cb?: (p: any) => void, args?: string): void => {

            let time: number = t || sToNum(TIME);
            let delay: number = 0;
            cover.style.visibility = 'visible';

            setTimeout(() => {
                Object.assign(patch.style, {
                    transform: 'scaleX(1)',
                })


            }, delay += time);

            // The first timeout covers the screen; this if(cb)timeout loads the content under the patch,
            // the next timeout removes the patch
            if (cb) setTimeout(() => {
                cb(args)
            }, delay + time);

            setTimeout(() => {

                Object.assign(patch.style, {
                    transform: 'scaleY(0)'
                })
            }, delay += time);

            setTimeout(() => {
                cover.parentElement!.removeChild(cover);
            }, delay + time);

        }

    }
})()
