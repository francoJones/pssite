/** ------ When adding to the model fragments that are self-populated somehow:
 - A fragment is a property from the model
 - Add the new fragment as an object, eg Fragment
 - Add it to const Model,
 const Model={
             ...
             ,fragment:Fragment
             ...
        }

 - Add a  buildModel function (eg:buildFragmentModel) which will return a promise
 as well as populate the fragment, and call it from buildModel, eg:

 buildFragmentModel(Fragment,host,isNative).then((fragment)=>{fragmentBuilt("fragment",fragment)})
 .catch((err)=>{console.log("ERROR buildFragmentModel",err)});

 - Add a reducer in Reducers.jsx, named as the fragment but lowercase (eg: Fragment will require a fragment() reducer)
 and add it to the export list.
 - Add to REDUCERS in Reducers.jsx.

 -----------------------------------------------------------------------------

 If a fragment can be written as a plain value, just add it:
 const Model={
             ...
             ,fragment1:null
             ,fragment2:[1,2,3]
             ,fragment3:10
             ,fragment4:someBigObjectWrittenInADifferentFileAndImportedHere
             ...
        }

 and pass it through fragmentBuilt

    fragmentBuilt('fragment1')
    fragmentBuilt('fragment2')
    ...

 */

import { sections } from './MSections';
import { gallery } from './MGallery';
import {model} from '@T/types'

// -- names must match the respective reducers --//

const Model:model = {
    sections,
    gallery
}


// -- List here the models with no properties created dynamically
const STATIC_FRAGMENTS_LIST:Array<string> = [
    'sections',
    'gallery'
]

// ----------------------------------------------//

let pending:number = Object.keys(Model).length;

function buildModel (cb:(p:model)=>void):void{
    /**
    Use build###Model only when the model is NOT hardcoded. E.g:
     - when waiting for server data
     - when the properties are created procedurally
     etc.

     Use fragmentBuilt always, so the model is frozen once complete.

     Right now build###Model is used everywhere because I still don´t know how things have to look

     **/

    // -- STATIC FRAGMENTS --
    for (const f of STATIC_FRAGMENTS_LIST)fragmentBuilt(f);
    // ---------------------------

    // TODO put these two on a separate function and call it when the model build is complete
    Object.freeze(Model);
    cb(Model);
    /** ------------------------------------------------------------------------------------ */
}

function fragmentBuilt (prop:string, val?:any):void {
    Model[prop] = val || Model[prop]
    pending--;
    if (pending !== 0) return;

    for (let k in Model)Object.freeze(Model[k]);
}

export { Model, buildModel }
