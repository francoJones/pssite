﻿<?php

include "php/connectToDB.php";
include "php/_functions.php";

define('DEFAULT_COPY', "user centered design and development");
/** USAGE
 * http://pacosamino.com/AddEntry.php?jobs=owl_s,proto,picas,ui&copy=user centered design
 **/

// e.g.:http://pacosamino.com?fetch=owl_s,horac,decom,aquab
// that should redirect to http://pacosamino.com/abc123
// where abc123 is an entry in the table masks that stores a list of ids (owl_s, horac, decom, aqua) to be retrieved from table sources.

if (isset($_GET["jobs"])) {

    $list = $_GET["jobs"];

    $exp = preg_split('/,/', $list, -1, PREG_SPLIT_NO_EMPTY);
    if (count($exp) == 0) return;
    $copy = isset($_GET["copy"]) ? $_GET["copy"] : "user centered design and development";

    $url_creator = new UrlCreator($db, $list, $copy);

}


class UrlCreator
{

    private $db;
    private $ovf = 0;
    private $short_url;

    public function __construct($db, $list, $copy)
    {

        // connects to the db
        $this->db = $db; //connectToDB();
        if (!$this->db) return; // no connection with the db? stop then.


        // creates and adds to the db the string that will be the shortened url, e.g.:abc123
        $short_url = $this->createRandomString($this->db);

        $is_entry_inserted = $this->insertEntry($this->db, $short_url, $list, $copy);

        if (!$is_entry_inserted) {
            br("No new entry has been created.");
            return;
        }

        $header = "location:http://pacosamino.com" . str_replace(basename($_SERVER['PHP_SELF']), $short_url, $_SERVER['SCRIPT_NAME']);
        //echo  $header;
        header($header);

        $this->short_url = $short_url;

    }


    private function createRandomString($db)
    {

        if ($this->ovf++ > 20) {
            br("overflow error");
            return;
        }

        $short_url = substr(str_shuffle(MD5(microtime())), 0, 5);

        // checks if the string already exists in the db
        $query = "SELECT * FROM masks WHERE short_url='$short_url'";
        $result = $db->query($query);
        if (!$result) {
            echo "There was a problem:<br />$query<br />{$db->error}";
            return;
        }

        if ($result->num_rows === 0)// this short url does not exist, it's good to go
            return $short_url;
        else    // it exists, so the process is repeated
            $this->createRandomString($db);


    }

    private function insertEntry($db, $short_url, $list, $copy)
    {

        if (count($list) == 0) {
            br("What kind of list was provided? Is it properly formatted?");
            return;
        }

        //---------------------------------------------------------------------------------------
        // the list is stored as indexes (1,2,3,4) instead of strings (owl_s, horac, decom, aqua)
        // in order to make db search more efficient, so a list of indexes is retrieved from the db
        // and put together in a string, as in "1,2,3,4"

        //-- index array creation
        $exp = preg_split('/,/', $list, -1, PREG_SPLIT_NO_EMPTY);
        $id_list = array();


        foreach ($exp as $v) {
            $q = "SELECT * FROM sources WHERE job='$v'";
            $r = $db->query($q) or die($db->error);
            $f = $r->fetch_object();

            if (!$f) {
                br("Entry <b>$v</b> not found in " . DB_NAME . ".");
                continue;
            }

            array_push($id_list, $f->id);

        }
        //--


        $id_list_imp = implode(",", $id_list);  // converting to string

        //-- adding to db
        $query = "INSERT INTO masks (short_url,list,copy) VALUES ('$short_url','$id_list_imp','$copy')";
        $result = $db->query($query) or die($db->error);
        //--

        //---------------------------------------------------------------------------------------


        return $result;

    }// end insertEntry

}// end class


?>
