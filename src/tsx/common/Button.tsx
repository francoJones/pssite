import * as React from 'react';
import * as css from '@scss/export.scss';

type props = {
    icon: React.ReactFragment;
    bgColor: string;
}

export default class Button extends React.PureComponent<props> {

    static defaultProps = {
        bgColor: css.red
    }

    constructor(props) {
        super(props);
    }

    render() {

        return <svg version='1.1'
                    xmlns='http://www.w3.org/2000/svg'
                    x='0px' y='0px'
                    viewBox='0 0 87 106.904' className='bt'>
            <polygon className='flag__border' fill='none' strokeWidth='10' strokeMiterlimit='10' points='5,60.861 82,98.861 82,46.043 5,8.043
			'/>
            <polygon fill={this.props.bgColor || Button.defaultProps.bgColor}
                     points='5,60.861 82,98.861 82,46.043 5,8.043 		'/>
            <g className='icon'>{this.props.icon}</g>
        </svg>

    }
}

