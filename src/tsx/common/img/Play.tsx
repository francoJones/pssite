import * as React from 'react'
import {playBanner, getVideo} from '@libs/Utils';
import Button from '@common/Button';
import {BANNERS, GALLERY} from '@consts/DB';



type props = {
    className?: string
    type: string | null,
    getPlayerContainer: () => HTMLElement,
    internalLink: string,
    behavior: typeof CASE | typeof GALLERY | typeof BANNERS
}

type state = {
    isPlaying: boolean;
    maxScale: number
}

const DEFAULT_MAXSCALE: number = 2;
const CASE: string = 'CASE';

export default class Play extends React.Component<props, state> {

    nodeRef!: React.RefObject<HTMLDivElement>;
    video: HTMLVideoElement | null = null;
    videoCont: HTMLElement | null = null;

    constructor(props) {
        super(props);
        this.nodeRef = React.createRef();
        this.state = {
            isPlaying: false,
            maxScale: DEFAULT_MAXSCALE
        }
    }

    static defaultProps = {
        className: 'play'
    }

    // TODO all the play video/banner logic should be either in Img or in a separate helper class/function
    // Right now Img is calling onClick from Img.onClick
    onClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent | PointerEvent>): void => {

        e.stopPropagation();

        if (this.props.type === BANNERS) {
            // TODO the play button does not reappear once the banner finishes, and there is no way to detect that.
            // In the meantime, banners replay just clicking on them again.
            // Maybe there should appear a "replay" button on a corner or whatever.
            const isPlaying = this.state.isPlaying;
            if (isPlaying) return;
            this.setState({isPlaying: !isPlaying});
            return;
        }

        let video: HTMLVideoElement | null = this.video;
        if (video === null) {
            const videoCont: HTMLElement = this.videoCont = this.props.getPlayerContainer() as HTMLElement;
            video = getVideo(this.props.internalLink)!;
            video.addEventListener('playing', this.onStartVideo);
            video.addEventListener('pause', this.onPauseVideo);
            const node = this.nodeRef.current!;
            node.style.pointerEvents = 'none';

            // TODO refactor all this =='GALLERY' mess from here to the end, maybe 2 separate functions for Gallery and Case
            if (this.props.behavior === GALLERY) {
                //-- some FX here
                Object.assign(videoCont.style, {
                    visibility: 'visible'
                    , height: '100vh'
                    , background: '#403329'
                });
                //--
            } else {
                videoCont.style.visibility = 'visible'
            }
            videoCont.appendChild(video);
            this.video = video;

            if (this.props.behavior === GALLERY || this.props.behavior === CASE) {
                // after the FX end, play video
                setTimeout(() => {
                    this.changePlayState(video!)
                }, this.props.behavior === GALLERY ? 300 : 0 /** based on TIME_3  */);
                return;
            }
        }

    }

    changePlayState(video?: HTMLVideoElement): void {

        const isPlaying = this.state.isPlaying;
        if (video)
            if (isPlaying)
                video.pause();
            else
                video.play();
        this.setState({isPlaying: !isPlaying})

    }


    componentWillUnmount = (): void => {
        const video: HTMLVideoElement | null = this.video;
        if (!video) return;
        video.removeEventListener('playing', this.onStartVideo);
        video.removeEventListener('pause', this.onPauseVideo);

    }

    onStartVideo = (): void => {
        this.setState({isPlaying: true});
    }

    onPauseVideo = (): void => {
        this.setState({isPlaying: false});
    }

    componentDidMount(): void {
        setTimeout(() => { // waiting for the actual render to happen
            if (this.props.getPlayerContainer().offsetHeight <= 120)
                this.setState({maxScale: DEFAULT_MAXSCALE});
        }, 100);
    }


    render() {

        const {className, behavior}: props = this.props;
        const maxScale: number = this.state.maxScale;
        // TODO that translateY(4px,1rem) has to be replaced by a formula
        const transformCont = {transform: ` scale(${this.state.isPlaying ? 0 : maxScale}) ${maxScale === DEFAULT_MAXSCALE ? 'translateY(-1rem)' : 'translateY(4px)'}`};
        const transformPlay = 'scale(1) translateY(-1rem)';

        const content = <div style={transformCont}>
            <Button icon={<React.Fragment>
                <g className='play' transform={transformPlay}>
                    <polygon fill="none" stroke="#FFFFFF" strokeWidth="2.8" strokeMiterlimit="10" points="36.786,42.328 53.821,51.542
		36.786,60.576 	"/>
                </g>

            </React.Fragment>
            }/></div>;


        if (behavior !== CASE)
            return <div className={className || Play.defaultProps.className} ref={this.nodeRef} onClick={this.onClick}>
                {content}
            </div>
        else
            return <div className={className || Play.defaultProps.className} ref={this.nodeRef}>
                {content}
            </div>


    }

}
