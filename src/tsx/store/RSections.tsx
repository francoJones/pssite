export {sections}
import {model, IActionVal} from '@T/types';

function sections(state: model | undefined, action) {
    if (typeof state === 'undefined') return null;

    const type: string = action.type
    const val: IActionVal = action.val;

    if (type !== 'sections') return state;

    if (val.prop === 'current') return Object.assign({}, state, {[val.prop]: val.val});
    return Object.assign({}, state, {[val.prop]: val.val});
}
