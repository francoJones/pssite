// This class triggers a recalculation of the page/chapter position, as well as their sizes. Has nothing to do with styling.
// Font size can be changed via browser. This triggers no events, so it has to be detected by hand.
// Also, resize events are triggered by the dozen per second, so an interval is set up, comparing
// sizes only a couple of times per second, and triggering only if size differs.
const INTERVAL: number = 600; // based on 60fps

export default class ResizeManager {

    target: any;
    cb: (p?: any) => void;
    scope: any | null;
    isResizing: boolean = false;
    time: number = 0;
    interval: number = 0;
    isFirstResize: boolean = true;
    w0: number = 0;
    h0: number = 0;

    constructor(cb: (p?: any) => void, scope: any, target: any = window) {
        this.target = target;
        this.cb = cb;
        this.scope = scope;

        window.addEventListener('resize', this.onResize);
        window.addEventListener('blur', this.onBlur);
        window.addEventListener('focus', this.onFocus);
    }

    onResize = (): void => {
        if (this.isResizing) return;

        setTimeout(() => {
            this.w0 = window.innerWidth;
            this.h0 = window.innerHeight;
            // @ts-ignore
            (this.cb).call(this.scope, [this.w0, this.h0]);
            this.isResizing = false;
        }, INTERVAL / (this.isFirstResize ? 5 : 1))
        this.isResizing = true;
        this.isFirstResize = false;
    }

    onBlur = (): void => {
        this.isFirstResize = true;
    }

    onFocus = (): void => {
        this.isFirstResize = true;
    }


    resizeLoop = (): void => {
        const target = this.target;
        if (target.offsetWidth !== this.w0 || target.offsetHeight !== this.h0) {
            // @ts-ignore
            (this.cb).call(this.scope, [window.innerWidth, window.innerHeight]);
            this.w0 = target.offsetWidth;
            this.h0 = target.offsetHeight;
        }
    }

    dispose(): void {
        clearInterval(this.interval);
        window.removeEventListener('resize', this.onResize);
        window.removeEventListener('blur', this.onBlur);
        window.removeEventListener('focus', this.onFocus);
    }
}
