import * as React from "react"
import Close from './Close';
import {Nav} from '@libs/Nav'
import Img from "@common/img/Img";
import {IImg} from "@T/types";
import {IMG} from '@consts/DB';


type props = {
    hash: string;
    dat: { [k: string]: any };
    onClose: (p?: () => void) => void;
}
type state = {
    isZoomed: boolean;
}

export default class Gallery extends React.Component<props, state> {

    static defaultProps = {
        imgList: []
    }

    nodeRef!: React.RefObject<HTMLDivElement>;
    state = {
        isZoomed: false
    }

    constructor(props) {
        super(props)
        Nav.addSection('gallery.Gallery', this.onClickClose, this);
        this.nodeRef = React.createRef();

    }


    componentDidMount(): void {

        let node: HTMLElement = this.nodeRef.current!.parentElement as HTMLElement;
        node.style.height = '100vh';
        node.style.visibility = 'visible'

    }

    onClickClose = (): void => {
        this.props.onClose(() => {
            let node: HTMLElement = this.nodeRef.current!.parentElement as HTMLElement;
            node.style.visibility = 'hidden';
            node.style.height = '0';
            Nav.goTo();
        });
    }

    onClickZoom = (): void => {
        this.setState((st) => {
            return {isZoomed: !st.isZoomed}
        });
    }

    render() {

        const {file, type, internal_link, internal_link_type}: { [k: string]: any } = this.props.dat;
        let baseClass: string = 'gallery';

        /**
         The db gives type and internal_link_type. Type affects all elements. By default is
         null, which means every item is an 'img'.
         internal_link_type overrides that value. Items that don't have it are assumed to take
         the global value, that is, type.
         The reason for this is dealing with mixed content jobs, such as 'db.sources.proto', where there
         is a mix of video and images.
         */
        const itemType: string = internal_link_type || type || IMG;
        /** -- **/

        let imgProps: IImg = {
            src: file,
            type: itemType,
            showText: false,
            internal_link,
            onClick: this.onClickClose,
            isZoomed: this.state.isZoomed
        }

        return <React.Fragment>
            <div className={'bg'} ref={this.nodeRef}></div>
            <Img {...imgProps} />
            <Close className={baseClass + '__close'} onClick={this.onClickClose}/>
            {/*itemType===IMG?
                <Zoom className={baseClass+'__zoom'}  onClick={this.onClickZoom} bgColor={css.beige40} isZoomed={this.state.isZoomed} />
                :null*/}//TODO zoom disabled, decide about adding it or what

        </React.Fragment>
    }

}

