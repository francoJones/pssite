﻿const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const webpack = require('webpack');
const SRC_DIR = path.resolve(__dirname, 'src');
const DIST_DIR = path.resolve(__dirname, 'src/dist');


module.exports =env =>{

    const isDev=env?env.isDev:false;
    console.log(isDev?'-- DEVELOPMENT --\n\nENSURE IS_DEV IS SET TO TRUE IN INDEX.PHP':'-- PRODUCTION --\n\nENSURE IS_DEV IS SET TO FALSE IN INDEX.PHP');

    return {
        mode: isDev ? 'development' : 'production',
        entry: SRC_DIR + '/tsx/index.tsx',
        devtool: 'source-map',
        output: {
            path: DIST_DIR,
            filename: 'index.js'
        },
        resolve: {
            extensions: ['.js', '.jsx', '.tsx', '.ts', '.scss'],
            plugins: [
                new TsconfigPathsPlugin()
            ]
        },
        module: {
            rules: [
                {
                    test: /\.ts(x?)$/,
                    exclude: [/node_modules/, /_ref/],
                    use: [
                        {
                            loader: "ts-loader"
                        }
                    ]
                },
                {
                    enforce: "pre",
                    test: /\.js$/,
                    loader: "source-map-loader"
                },
                /*{
                    test: /\.(css|scss)$/,
                    use: ['style-loader', 'css-loader', 'sass-loader']
                }*/
                {
                    test: [/\.s[ac]ss$/i,/\.css$/i],
                    use: [
                        // Creates `style` nodes from JS strings
                        'style-loader',
                        'css-loader',
                        // Compiles Sass to CSS
                        {
                            loader: 'sass-loader',
                            options: {
                                // Prefer `dart-sass`
                                implementation: require('sass'),
                                sourceMap: isDev?true:false,
                                sassOptions: {
                                    outputStyle: isDev?'expanded':'compressed',
                                    data: '@import "mixins";',
                                    includePaths: [SRC_DIR+'/scss'],
                                }

                            },
                        },

                    ],
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
                    use: [
                        'file-loader',
                    ],
                },

            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                __IS_DEV__:JSON.stringify(isDev)
            })
            ],

        target: 'web',
        watchOptions: {
            ignored: /node_modules/
        }
    }
};
