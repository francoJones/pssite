export {gallery}
import {model} from '@T/types';

function gallery(state: model | undefined, action) {
    if (typeof state === 'undefined') return null;

    const type: string = action.type;
    const val = action.val;

    if (type !== 'gallery') return state;

    const prop = val.prop;
    if (prop === 'current') {

        const current = val.val;
        if (current === null) return Object.assign({}, state, {[prop]: -1});
        return Object.assign({}, state, {[prop]: Number(current)});
    }

    return Object.assign({}, state, {[prop]: val.val});
}
