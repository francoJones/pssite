<?php


Model::init();


class Model
{

    const FALLBACK_IN_CASE_OF_TOTAL_FAILURE = "user centered design and development";
    const DEFAULT_SHORT_URL = "DEF"; // "DEF" is an arbitrary value in table "masks"
    private static $short_url;
    private static $copy;
    private static $claim;
    private static $mask;

    public static function init()
    {

        // if no mask is provided, DEFAULT_SHORT_URL is used for querying the db
        self::$mask = isset($_GET["mask"]) ? $_GET["mask"] : NULL;

        include "connectToDB.php";

        //-- gets claim from table masks --//
        $query = "SELECT * FROM masks WHERE short_url='" . substr(self::$mask, 0, 5) . "'";

        $result = $db->query($query);

        //-- in case of db failure
        if ($result == NULL) {
            self::$short_url = NULL;
            self::$copy = self::FALLBACK_IN_CASE_OF_TOTAL_FAILURE;
            return;
        }
        //--

        $entry = $result->fetch_object();

        //-- in case of a mask that does not exist in the db (e.g. 1234567)
        // OR
        //-- if no mask was provided
        if ($entry == NULL || self::$mask == NULL) {
            self::$short_url = NULL;
            $query = "SELECT * FROM masks WHERE short_url='" . self::DEFAULT_SHORT_URL . "'";
            $result = $db->query($query);
            $entry = $result->fetch_object();
            self::$copy = $entry->copy;
            return;
        }
        //--

        self::$short_url = self::$mask;
        self::$copy = $entry->copy;

    }


    public static function getCopy()
    {
        return self::$copy;
    }

    public static function getShortUrl()
    {
        return self::$short_url;
    }


}


?>
