import {lockSize} from './Utils'
import {TIME_5 as TIME} from '@consts/Transforms';

const TRIM = '.clipFX__trim' // TODO put this as a class on the container that determines the visible part
const CB_TIME_OFFSET_MULT = 150;

function sToNum(n: string): number {
    return Number(TIME.replace('s', '')) * 1000
}

export default class CoverFX {

    list;

    buildIntro(cont: HTMLElement): void {
        this.list = cont.querySelectorAll(TRIM);
        const list = this.list;
        for (const node of list) {
            Object.assign(node.style, {
                visibility: 'hidden', // for mobile
                position: 'relative',
                width: '.1%',
                height: '100%',
                overflow: 'hidden',
                transition: `width ${TIME} ease-in-out`
            })
        }
    }

    runIntro = (t?: number, singleCb?: (p: HTMLElement) => void, singleCbDelayMult: number = 1, totalCb?: () => void, totalCbDelayMult: number = 1): void => {
        /**
         singleCb is a callback used every time an instance of ClipTextFX is done
         total Cb is a callback used when every instance of ClipTextFX is done
         **/
        let time: number = t || sToNum(TIME);
        const list = this.list;
        let delay: number = 0;
        for (let node of list) {
            // TODO put this timeouts in an array, to stop them in case the outro runs too soon
            setTimeout(() => {
                Object.assign(node.style, {
                    visibility: 'visible',
                    width: '100%'
                })
            }, delay += time);

        }

        // these are the actions when the transition is complete,
        // does not rely on transitionend for reliability and simplicity
        setTimeout(() => {
            for (const node of list) {
                if (singleCb) singleCb(node);
            }
        }, (delay += CB_TIME_OFFSET_MULT) * singleCbDelayMult);

        if (totalCb) {
            setTimeout(() => {
                totalCb();
            }, delay * totalCbDelayMult);
        }
    }

    runOutro = (time?: number | null, cb?: (p?: any) => void): void => {

        const list = this.list;
        let delay: number = 0;
        for (const node of list) {
            lockSize(node, true);
            setTimeout(() => {
                Object.assign(node.style, {
                    width: '0.1%',
                    overflow: 'hidden'
                });

                if (cb)
                    setTimeout(() => {
                        cb();
                    }, sToNum(TIME))
            }, delay += (time ? time : 100));
        }
    }
}
