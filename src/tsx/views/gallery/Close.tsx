import * as React from 'react'
import Button from '@common/Button';

type props = {
    className?: string,
    onClick: () => void;
}


export default class Close extends React.Component<props> {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        className: 'close'
    }

    onClick = () => {
        if (this.props.onClick) this.props.onClick()
    }

    render() {

        return <div className={this.props.className || Close.defaultProps.className} onClick={this.onClick}>

            <Button
                icon={
                    <React.Fragment>
                        <line fill='none' stroke='#FFFFFF' strokeWidth='4' strokeMiterlimit='10' x1='35.267' y1='60.131'
                              x2='51.733' y2='43.665'/>
                        <line fill='none' stroke='#FFFFFF' strokeWidth='4' strokeMiterlimit='10' x1='51.733' y1='60.131'
                              x2='35.267' y2='43.665'/>
                    </React.Fragment>
                }/>


        </div>
    }
}
