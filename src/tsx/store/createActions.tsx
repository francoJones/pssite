import {model} from '@T/types'

export {createActions}

function createActions(M: model, store): { [k: string]: any } {
    const list: { [k: string]: any } = {}

    const suffix = (entries, fragment) => {
        for (const entry of entries) {

            // each entry is an array key-value. Only the key (entry[0]) has use here
            // -- thisPropName is converted to const case THIS_PROP_NAME --//
            const match: Array<string> = entry[0].match(/([A-Z]?)([a-z]*)/g);
            match.pop(); // removes the last instance, which is empty
            const action = fragment.toUpperCase() + '_' + match.join('_').toUpperCase();
            // -------------------------------------------------------------//
            /** -- FILTER OR ADD CUSTOM ACTIONS AS REQUIRED HERE -- */
            // -- default --//
            list[action] = (val) => {
                const type = fragment;
                const prop = entry[0];
                store.dispatch({
                    type,
                    val: {prop, val}
                })

                Object.freeze(list[action]);
            }
            // --------------//
            /** ---------------------------------------------------**/
        }// end for entry
    }

    const entries = Object.entries(M);

    let entry: [string, any];
    for (entry of entries) {


        if (entry[1] === null) {
            list[entry[0].toUpperCase()] = (val) => {
                store.dispatch({
                    type: entry[0],
                    val
                })
            }

            continue;
        }

        if (Object.entries(entry[1]).length > 0) {
            suffix(Object.entries(entry[1]), entry[0]);
            continue;
        } else {
            list[entry[0].toUpperCase()] = (val) => {
                store.dispatch({
                    type: entry[0],
                    val
                })
            }
        }
    }// end for entry

    return list;

}
