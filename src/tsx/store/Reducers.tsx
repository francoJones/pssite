import {sections} from './RSections';
import {gallery} from './RGallery';


const REDUCERS = {sections, gallery};

export {REDUCERS}
