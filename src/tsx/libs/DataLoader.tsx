function loadData(URL: string, cb: (p: { [k: string]: any }) => void, parse: boolean = true): void {
    /**
     parse==true : encodes the response as JSON
     */

    let req: any;
    try {
        // Opera 8.0+, Firefox, Safari
        req = new XMLHttpRequest();
    } catch (e) {
        // Internet Explorer Browsers
        try {
            // @ts-ignore
            req = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (e) {
            try {
                // @ts-ignore
                req = new ActiveXObject('Microsoft.XMLHTTP');
            } catch (e) {
                // Something went wrong
                console.log("couldn't retrieve server data");
                return;
            }
        }
    }

    req.onreadystatechange = function () {
        if (req.readyState === 4) {
            cb(parse ? JSON.parse(req.responseText) : req.responseText);
        }
    }

    req.open('GET', URL, true);
    req.send(null);
    // ------------------------------------------//
}

export {loadData}
