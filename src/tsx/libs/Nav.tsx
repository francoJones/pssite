/**
 * Initialise in root with Nav.init([GUID], [callback function:void], [scope]);
 * Navigations flows through callbacks to sections and links using addSection or addLink
 */

export type Tnav = {
    next: string,
    prev: string
}

interface Ihandler {
    fn: (p?: Tnav) => void,
    scope: any,
    id: string
}

interface IgoToHandler extends Ihandler {
    fn: (p?: any) => void
}


export const Nav = (() => {

    const history: Array<string> = []
    let linkList: { [k: string]: Ihandler } = {}
    let currentSection: Ihandler | null = null;
    let entryHash: string = '';
    let goToHandler: IgoToHandler;

    function getHash(): string {
        return window.location.hash.replace('#', '');
    }

    function onHashChange(e: Event): void {
        //console.warn('hash',window.location.hash,' prev:',history[history.length-1]);
        const next: string = getHash();
        const prev: string = history[history.length - 1]
        if (next === prev) return;
        history.push(next);
        if (currentSection !== null) {
            currentSection.fn.call(currentSection.scope, {
                next,
                prev
            });
            return
        }
        dispatch(linkList, next, prev)


    }


    function dispatch(handlerList: { [k: string]: Ihandler }, next: string, prev: string = '', isFirstEnter: boolean = false) {

        for (const handler of Object.values(handlerList)) {
            // TODO because of transitions, handler might be gone before this is triggered
            // Check for a better way to deal with that instead of if(handler)
            if (!handler) continue;
            if (next === '') continue;
            if (next === handler.id) {
                handler.fn.call(handler.scope, {
                    next,
                    prev
                });
                return
            }


        }
    }

    function changeHash(hash: string, fromGoto: boolean = false): void {
        window.location.hash = hash;
        history.push(hash);
    }


    return {
        // suggestion: call Nav.init on componentDidMount
        init: (id: string, fn: (p: any) => void, scope: any, deferFirstEnter: boolean = false): void => {
            goToHandler = {fn: fn, scope: scope, id: id}
            window.addEventListener('hashchange', onHashChange);
            entryHash = getHash();

            history.push(entryHash)
            if (!deferFirstEnter) dispatch(linkList, entryHash, '', true);

        }
        , goTo: (hash: string = ''): void => {
            // No parameters (as in Nav.goTo()) means go back to home page.
            if (hash === '') currentSection = null;
            const {fn, scope, id}: IgoToHandler = goToHandler;
            fn.call(scope, hash);
            changeHash(hash, true)
        },
        // If the splash has an intro, wait for it to finish, then call firstEnter
        firstEnter: (): void => {
            dispatch(linkList, entryHash)

        },
        addLink: (id: string, fn: ((p?: Tnav) => void), scope: any): void => {
            linkList[id] = {fn, scope, id}
        },
        addSection: (id: string, fn: (p?: Tnav) => void, scope: any): void => {
            currentSection = {fn, scope, id}
        },
        getHash: (): string => {
            return getHash();
        },
        getCurrent: (): string => {
            return getHash().split('/').pop() || '';
        },
        getPath: (): Array<string> => {
            return getHash().split('/');
        }
    }

})
()
