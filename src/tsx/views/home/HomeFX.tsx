import {LAZY_FX} from "@consts/ClassNames";
import {InScroll} from "@libs/InScroll";
import {CoverFX} from "@libs/CoverFX";
import {Nav} from '@libs/Nav';


export default class HomeFX {

    node: HTMLElement = {} as HTMLElement;

    lazyLoadUpdated = (img: HTMLElement): void => { // TODO check code duplication with CaseFX
        let parent: HTMLElement | null = img;
        while (!parent.classList.contains(LAZY_FX)) {
            parent = parent.parentElement;
            if (!parent) break;
        }
        if (!parent) return;
        parent.classList.add('done');
        InScroll.update();

    }

    runIntro = (navHash: string) => {

        CoverFX.buildIntro();
        CoverFX.runIntro(0, () => {
            (document.querySelector('.home') as HTMLElement).style.display = 'none';
            Nav.goTo(navHash);
        })
    }

    runOutro = (cb?: (p?: any) => void, args?: string) => {
        CoverFX.runOutro(0, () => {
            (document.querySelector('.home') as HTMLElement).style.display = 'block';
            if (cb) cb(args);
            Nav.goTo();
        })
    }

}
