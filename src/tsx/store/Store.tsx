import {REDUCERS} from './Reducers';
import {createActions} from './createActions';
import {buildModel} from './Model';
import {loadData} from '@libs/DataLoader';
import {MODEL_URL} from '@consts/URLs';
import {model} from '@T/types'
import {createStore, combineReducers, compose} from 'redux';

export var SET: { [k: string]: any } = {}
declare var __IS_DEV__: string;

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}


export function buildStore(shortUrl: string = '', cb?: (s: any) => void): void {

    let store: { [k: string]: any };

    loadData(MODEL_URL + '?short_url=' + shortUrl, onDataLoaded);


    function onDataLoaded(dat: { [k: string]: any }): void {

        const composeEnhancers: any = __IS_DEV__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;
        const enhancer = getEnhancer(navigator.userAgent, composeEnhancers)

        const combo = combineReducers(REDUCERS);

        buildModel((M: model): void => {

            // @ts-ignore
            store = createStore(combo, M, enhancer);

            SET = createActions(M, store);

            /** POPULATE DYNAMIC PARTS OF THE MODEL HERE **/
            SET.SECTIONS_DAT(dat);
            Object.freeze(SET);
            /*** ----------------------------------------------------------**/
        }); // in Model.jsx


        if (cb) cb(store);
    } // end onDataLoaded

    function getEnhancer(ua: string, composeEnhancers) {
        if (ua.indexOf('Android') !== -1 && ua.indexOf('Chrome') !== -1) return composeEnhancers;
        if (ua.indexOf('Firefox') !== -1) return composeEnhancers;
        if (ua.indexOf('Edge') !== -1) return composeEnhancers;
        return composeEnhancers();
    } // end getEnhancer

}

