import {TIME_5 as TIME} from "@consts/Transforms";

export default class LetterFX {

    spanList: Array<HTMLSpanElement> = [];

    constructor(selectorList: Array<string>) {

        let nodeList: Array<HTMLElement> = []
        let spanList: Array<HTMLSpanElement> = []

        //-- gets the nodes containing the strings
        for (let selector of selectorList) {
            let node: HTMLElement | null = document.querySelector(selector)
            if (node)
                nodeList.push(node);
            else
                throw `${selector} not found`
        }
        //--

        //-- replace the text with chains of spans
        for (let node of nodeList) {

            let chain: Array<HTMLSpanElement> = this.createSpanChain(node);
            spanList = spanList.concat(chain);

        }
        //--

        this.spanList = spanList;

    }

    runIntro(cb?: (p?: any) => void) {

        let delay: number = 0;
        let step: number = 10;
        let totalTime = 0
        for (let span of this.spanList)
            setTimeout(() => {
                span.style.transform = 'translateY(0)';
            }, delay += step);
        totalTime += (500 + step)
        if (cb) setTimeout(() => {
            cb()
        }, totalTime);


    }

    createSpanChain(node: HTMLElement): Array<HTMLSpanElement> {
        let str: string = node.innerText.replace(new RegExp(/\n/g), ' / ');
        let ret: Array<HTMLSpanElement> = [];
        let height = Number((window.getComputedStyle(node)['font-size']).replace('px', ''))
        let offsetY: number = height * 1.2;
        // words are created so the sentences wrap properly when the window size changes
        let word: HTMLElement = this.createWord();
        //--
        node.textContent = '';
        for (let letter of str) {
            let span = document.createElement('span');
            span.innerHTML = (letter === ' ' ? '&nbsp;' : letter);
            Object.assign(span.style, {
                display: 'inline-block',
                transform: `translateY(${offsetY}px)`,
                transition: `transform ${TIME} ease-in-out`
            });
            word.append(span);
            if (letter === ' ') {
                node.append(word);
                word = this.createWord();
            }
            ret.push(span);
        }
        node.append(word)
        return ret;
    }// end createSpanChain

    createWord(): HTMLElement {
        let word = document.createElement('div');
        Object.assign(word.style, {
            padding: '.4rem 0',
            overflowY: 'hidden',
            position: 'relative',
            display: 'inline-block'

        });
        return word;
    }

}

