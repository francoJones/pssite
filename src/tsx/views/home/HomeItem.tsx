import * as React from "react"
import ViewCase from "@views/home/ViewCase";
import {Nav, Tnav} from '@libs/Nav'
import {LAZY, LAZY_FX} from '@consts/ClassNames';

type props = {
    className: string,
    onClick: (p: string) => void,
    entry: { [k: string]: any },
    hash: string
}

export default class HomeItem extends React.PureComponent<props> {

    title: string;

    constructor(props) {
        super(props);
        this.title = props.entry.title || 'ADD THIS TITLE';
        Nav.addLink(this.props.hash, this.onNav, this);
    }

    onNav = (p?: Tnav): void => {
        this.onClick();
    }


    onClickCase = (): void => {

        let entry: { [k: string]: any } = this.props.entry;

        if (entry.type === 'case') {
            this.onClick();
            return
        }
        let link: string = entry.link;
        if (link !== '') {
            window.open(link, '_blank');
            return;
        }
        this.onClick();
    }

    onClick = (): void => {
        this.props.onClick(this.props.hash);
    }

    getRole = (role: string): string => {
        //TODO replace <br> with spans in the db, remove this function.
        let split: Array<string> = role.split('<br>');
        return `${split.join(' / ')}`;
    }

    getViewCaseText(type?: string, link?: string): string {

        if (link) return 'Visit site';

        switch (type) {
            case 'case':
                return 'View case';
            case 'vid':
                return 'View video';
            default:
                return 'Enlarge';
        }

    }

    render() {

        const {className, entry}: props = this.props;
        const {role, description, description_short, file, caption, type, link} = entry;

        const viewCaseText: string = this.getViewCaseText(type, link);

        let roleFixed: string = role ? this.getRole(role) : '';

        let desc: string = description_short ? description_short :
            description ? description :
                caption ? caption : '';
        // TODO refactor the description mess at div item__description
        return <div className={`${className}__item ${LAZY_FX}`}>
            <div className={'item__left-block'}>
                <div className={'item__title'} dangerouslySetInnerHTML={{__html: this.title}}
                     onClick={this.onClickCase}/>
                {role ? <div className={'item__role'} dangerouslySetInnerHTML={{__html: roleFixed}}/> : null}
                <div className={'item__description'} dangerouslySetInnerHTML={{__html: desc}}/>
                <div className={'item__view-case'} onClick={this.onClickCase}>
                    <ViewCase text={viewCaseText}/>
                </div>
            </div>
            <div className={'item__right-block'} onClick={this.onClickCase}>
                <picture><img className={LAZY} data-src={file} src={'im/placeholder.gif'}/></picture>
            </div>

        </div>


    }

}
