export function getMatrix(node): { [k: string]: any } {
    const st: { [k: string]: any } = window.getComputedStyle(node)!;
    return st.transform.match(/([-]*[.]*[0-9])+/g).map((v) => Number(v));
}

// TODO rename to lockWidth
export function lockSize(node: HTMLElement, lockRoot = false) {
    const pList: any = node.children;

    for (const child of pList) {
        Object.assign(child.style, {
            width: child.offsetWidth + 'px'
        })
    }

    if (!lockRoot) return;
    Object.assign(node.style, {
        width: node.offsetWidth + 'px'
    })
}

export function unlockSize(node, unlockRoot = false): void {
    const pList: any = node.children;
    const props: { width: string } = {
        width: ''
    }
    for (const child of pList) {
        Object.assign(child.style, props)
    }

    if (!unlockRoot) return;
    Object.assign(node.style, props)
}

export function playBanner(node: HTMLElement, container: HTMLElement, src: string): void {
    //-- the path to the banner is figured out from the src path of the poster image
    const im: HTMLImageElement = node.querySelector('img')!
    const noExt = src.split('.');
    noExt.pop();
    const folderPath: string = noExt.join('.') + '/';
    //--

    const iframe: HTMLIFrameElement = document.createElement('iframe');
    // @ts-ignore
    iframe.width = im.width;
    // @ts-ignore
    iframe.height = im.height;
    iframe.frameBorder = '0';
    iframe.scrolling = 'no';

    iframe.src = folderPath + 'index.html';

    const prev: HTMLIFrameElement = node.querySelector('iframe')!;
    if (prev) prev.parentElement!.removeChild(prev);
    container.appendChild(iframe);
}

export function getVideo(noExtFile: string, style?: { [k: string]: any }): HTMLVideoElement {

    const vid: HTMLVideoElement = document.createElement('video');
    const mp4: HTMLSourceElement = document.createElement('source');
    mp4.src = noExtFile + '.mp4';
    const webm: HTMLSourceElement = document.createElement('source');
    webm.src = noExtFile + '.webm';
    vid.appendChild(mp4);
    vid.appendChild(webm);
    // @ts-ignore
    vid.setAttribute('preload', true);
    // @ts-ignore
    vid.setAttribute('controls', false);
    Object.assign(vid.style, style ? style : {
        width: '100%',
        height: 'auto'
    })

    return vid;
}

