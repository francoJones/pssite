'use-strict'

import Img from '@common/img/Img';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Arrow from './Arrow'
import {Nav} from '@libs/Nav'
import LazyLoad from 'vanilla-lazyload';
import CaseFX from './CaseFX';
import {IImg} from '@T/types';
import {BANNERS, FILE} from '@consts/DB';
import {InScroll} from '@libs/InScroll';
import ResizeManager from '@libs/ResizeManager';
import {LAZY} from '@consts/ClassNames';


type props = {
    onClick?: (p: number) => void,
    dat: { [k: string]: any },
    className?: string,
    hash: string,
    onClose: () => void;
}

/** for reference
 * IImg{
    index?: number,
    src: string,
    onClick?: (p: number) => void,
    type?: string,
    className?: string,
    showText:boolean
}*/


interface IItemProps extends IImg{
    key: number,
    flex?: string,
}

type state = {
    isReady: boolean
}

export default class Case extends React.PureComponent<props, state> {

    contentRef!: React.RefObject<HTMLDivElement>;
    contentList: Array<JSX.Element>;
    state = {isReady: false}
    lazyLoad: any;
    fx:CaseFX ={} as CaseFX;

    static defaultProps = {
        type: '',
        className: 'content',
        palette: {bg: 'blue'},
        onClick: () => console.warn('default onClick')
    }

    constructor(props) {
        super(props);

        this.contentRef = React.createRef();
        const {dat, className}: props = props;
        this.contentList = this.getContentList(dat);

        this.onClickArrowTop = this.onClickArrowTop.bind(this);
        Nav.addSection('case.Case', this.onClickArrowBack, this);

    }


    componentDidMount() {

        this.fx= new CaseFX('.case');
        this.fx.buildIntro();

        this.lazyLoad = new LazyLoad({
            elements_selector: `.${this.props.className}__content .${LAZY}`,
            callback_loaded:this.fx.lazyLoadUpdated

        });
        window.scroll(0, 0);
        //// TODO remove this
    if(navigator.userAgent.toLowerCase().indexOf('firefox')!==-1)
        {
            const rm = new ResizeManager(InScroll.update, InScroll);
            setTimeout(()=> {
               //InScroll.update(true);
            rm.onResize();
            //console.log(document.body)
           },25);}



    }


    onClickContent=(p?:any):void=>{
        // TODO zoom images on click
        //console.warn('onClickContent',p,typeof p,this);
    }

    getContentList(dat: { [k: string]: any }): Array<JSX.Element> {

        let className:string=Case.defaultProps.className
        const list: Array<JSX.Element> = [];

        const conf: { [k: string]: any } = dat.conf;
        let index: number = 0;
        let rowDivList: Array<JSX.Element> = [];
        let addToRowDiv: boolean = false;
        const srcList = dat.conf ? dat.conf.file : dat.gallery_img_list;

        for (const src of srcList) {

            // -- text-only node
            if (conf.text_full_row)
                if (conf.text_full_row[index]) {
                    list.push(<div className={className + '__item'} key={'row_' + index}>
                        <div className={'item__text-full-row'}
                             dangerouslySetInnerHTML={{__html: conf.text_full_row[index]}}/>
                    </div>);
                    index++;
                    continue;
                }
            /// --

            let props:IItemProps={} as IItemProps;

            for (const [k, v] of Object.entries(conf)) {
                if (k === FILE) continue;
                props[k] = v[index];
            }

            //-- Dynamic props that cannot be assigned by the db
            Object.assign(props,{
                index,
                src,
                onClick: this.onClickContent || Case.defaultProps.onClick,
                type: conf.type===BANNERS?BANNERS:conf.type[index],
                key: index,
                className: `${className}__item`,
                showText:true
            });
            //--

            if (props.flex === 'start')addToRowDiv = true;
            if (addToRowDiv){
                props.className='row__item';
                rowDivList.push(<Img {...props} />);
            }
            if (props.flex === 'end') {
                list.push(<div className={className + '__item row'} key={'row_' + index}>{rowDivList}</div>);
                rowDivList = [];
                addToRowDiv = false;
                index++;
                continue;
            }

            if (!addToRowDiv) list.push(<Img {...props} />);
            index++;
        }// end for (const src of srcList)

        return conf.type===BANNERS?[<div className={className+'__banners'} key={'banner_list'}>{list}</div>]:list;
    }


    onClickArrowTop(): void {
        window.scroll(0,0);
    }

    onClickArrowBack = () => {
        this.props.onClose()
    }

    render() {
        let type:string=this.props.dat.type;
        let className: string = this.props.className || Case.defaultProps.className
        className = type === BANNERS ? className + '-banners' : className;
        const {title, description, role} = this.props.dat;

        return <React.Fragment>
            <div className={className + '__header'}>
                <div className='header__title'>
                    <div className='title__job' dangerouslySetInnerHTML={{__html: title}}/>
                    <div className={'title__role'} dangerouslySetInnerHTML={{__html: role}}/>
                </div>
                {ReactDOM.createPortal(
                    <Arrow className={'header__arrow-back'} onClick={this.onClickArrowBack} />,
                    document.querySelector('body') as Element)}

            </div>

            <div className={className + '__content'} ref={this.contentRef}>
                <div className={'content__description'} dangerouslySetInnerHTML={{__html: description}}/>
                {this.contentList}
            </div>

            <div className={className+'__footer'} >
                <Arrow onClick={this.onClickArrowTop} type='TOP' className='footer__arrow'/>
            </div>

        </React.Fragment>
    }
}
