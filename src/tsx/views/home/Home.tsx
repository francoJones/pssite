import HomeItem from '@views/home/HomeItem';
import * as React from 'react'
import * as ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import {ISections, model} from '@T/types'
import {SET} from '@store/Store';
import Case from '@views/case/Case';
import Gallery from '@views/gallery/Gallery';
import {Nav, Tnav} from '@libs/Nav';
import LazyLoad from 'vanilla-lazyload';
import {InScroll} from '@libs/InScroll';
import ResizeManager from '@libs/ResizeManager';
import HomeFX from './HomeFX'
import {CASE} from '@consts/DB';
import {LAZY} from '@consts/ClassNames';

type props = {
    sections: ISections
}


class Home extends React.Component<props> {

    caseCont: HTMLElement; // hardcoded div in index.php
    galleryCont: HTMLElement; // hardcoded div in index.php
    homeList: Array<JSX.Element> = [];
    lazyLoad: any;
    scroll: number = 0;
    rm: ResizeManager;
    fx: HomeFX;

    constructor(props) {
        super(props)
        this.caseCont = document.querySelector('.case') || document.createElement('div');
        this.galleryCont = document.querySelector('.gallery') || document.createElement('div');
        this.homeList = this.getHomeList(this.props.sections.dat);
        this.fx = new HomeFX();
        this.rm = new ResizeManager(InScroll.update, InScroll);
    }

    componentDidMount(): void {

        Nav.init('home', SET.SECTIONS_CURRENT, this);

        this.lazyLoad = new LazyLoad({
            elements_selector: '.' + LAZY,
            callback_loaded: this.fx.lazyLoadUpdated
        });

        setTimeout(() => {
            InScroll.init();
        }, 25)

    }


    componentDidUpdate(pr, st) {

        if ((pr.sections.current !== this.props.sections.current) && this.props.sections.current === '') {
            InScroll.update();
            // TODO get the clicked component, scroll to it rather than to the previous scroll position
            setTimeout(() => {
                window.scroll(0, this.scroll);
            })
        }
    }

    getHomeList(dat: any): Array<JSX.Element> {

        let list: Array<JSX.Element> = [];
        const contentClassName: string = 'home';

        let index = 0;

        for (const entry of Object.entries(dat)) {

            list.push(<HomeItem
                className={contentClassName}
                key={index}
                onClick={this.onClickItem}
                entry={entry[1] as { [k: string]: any }}
                hash={entry[0]}
            />);

            index++;
        }

        return list;
    }


    onClickItem = (navHash: string): void => {
        this.scroll = window.scrollY;
        this.fx.runIntro(navHash);

    }


    viewCase(currentSection: string, dat?: { [k: string]: any }): JSX.Element | null {
        if (!dat) return null;

        if (dat.type === CASE)
            return ReactDOM.createPortal(<Case hash={currentSection}
                                               onClose={this.onCloseSection}
                                               className={'case'}
                                               dat={dat}
            />, this.caseCont);
        else
            return ReactDOM.createPortal(<Gallery hash={currentSection} dat={dat}
                                                  onClose={this.onCloseGallery}/>, this.galleryCont);
    }

    onCloseGallery = (cb?: () => void): void => {
        this.onCloseSection(cb);
    }


    onCloseSection = (cb?: () => void): void => {
        this.fx.runOutro(() => {
            if (cb) cb();
            InScroll.update()
        });
    }


    render() {
        const {current, dat}: ISections = this.props.sections;
        if (!dat) return;

        let currentDat: { [k: string]: any } = dat[current];

        return (<React.Fragment>
                {this.viewCase(current, currentDat)}
                {this.homeList}
            </React.Fragment>
        )
    }
}

function mapStateToProps(state: model) {
    return {
        sections: state.sections
    }
}

export default connect(mapStateToProps)(Home);
