// $global-rotation (10)*PI/180=.174532 // 10 deg rotation
// note that CSS rotation axis is object-based, javascript rotation is screen-based
// these are screen-based values.
export const ROT_D: number = -5;
export const ROT: number = ROT_D * Math.PI / 180;//-0.174532
export const TRANS_3: string = 'transform .3s';
export const TRANS_5: string = 'transform .5s';
export const TIME_5: string = '.5s'
export const TIME_3: string = '.3s'
export const TIME_2: string = '.2s'
