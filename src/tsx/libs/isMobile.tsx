export {isMobile}

function isMobile(): boolean {
    // TODO  a better way to detect platforms
    let isMobile = false;
    const ua = window.navigator.userAgent;
    const modelList: Array<string> = ["iPhone", "iPad", "Android"];
    for (const model of modelList) if (ua.indexOf(model) !== -1) isMobile = true;

    return isMobile;

}
