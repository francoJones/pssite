const SELECTOR: string = '.i-scroll';
const SPEED: number = .1;

// @ts-ignore
const requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
// @ts-ignore
const cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelAnimationFrame;
type itemT = {
    el: HTMLElement,
    speed: number,
    y: number,
    isWrap?: boolean
}


export const InScroll = (
    function () {


        let isMobile: boolean = checkMobile();
        let raf: number;
        let body: HTMLBodyElement = document.querySelector('body')!;
        let wrap: HTMLElement = document.querySelector(SELECTOR)! as HTMLElement;
        let itemList: Array<itemT> = [];

        let f: number = 0

        function scroll(): void {
            raf = requestAnimationFrame(scroll);

            let sc1 = window.scrollY;
            for (let item of itemList) {

                let {y, el, speed, isWrap}: itemT = item;
                let av = (sc1 - y) * speed;
                /** fake parallax **/
                if (!isWrap) {
                    y += av;
                    el.style.transform = `translateY(${((av * av) < .005) ? 0 : -av * 2}px)`;
                    item.y = y
                    continue;
                }
                /**---------------**/


                if ((av * av) < .005) {
                    el.style.transform = `translateY(${-sc1}px)`;
                    item.y = sc1;
                    return
                }

                y += av;
                el.style.transform = `translateY(${-y}px)`;
                item.y = y

            }

        }

        function checkMobile(): boolean {
            let ua: string = navigator.userAgent
            return (ua.indexOf('iPhone') !== -1) || (ua.indexOf('Android') !== -1) || (ua.indexOf('Mobile') !== -1);
        }

        return {
            init: function (): void {
                // TODO the parallax thing can be refactored a bit,
                // like adding the 2nd selector as a parameter, a single itemList loop, etc
                itemList.push({
                    el: wrap,
                    speed: isMobile ? SPEED * 3 : SPEED,
                    y: 0,
                    isWrap: true
                })
                //};
                wrap.style.position = 'fixed';
                body.style.height = wrap.clientHeight + 'px';

                raf = requestAnimationFrame(scroll);


                /** fake parallax **/
                const nodeList: NodeListOf<Element> = document.querySelectorAll('.item__img-bt');
                for (const item of nodeList) {

                    itemList.push({
                        el: item as HTMLElement,
                        speed: (isMobile ? SPEED * 3 : SPEED) * Math.random() + .005,
                        y: 0
                    })
                }
                /** ------------**/


            },


            stop: function (): void {
                cancelAnimationFrame(raf);
            },

            kill: function (): void {
                stop();
                body.style.height = 'auto';
            },
            // Call this every time the content height changes
            update: function (updateItemList?: boolean): void {
                if (updateItemList) {
                    /** fake parallax **/
                    let nodeList: NodeListOf<Element> = document.querySelectorAll('.item__img-bt');
                    for (let item of nodeList) {

                        itemList.push({
                            el: item as HTMLElement,
                            speed: (isMobile ? SPEED * 3 : SPEED) * Math.random() + .005,
                            y: 0
                        })
                    }
                    /** ------------**/
                }
                body.style.height = wrap.clientHeight + 'px';
            }


        }
    }
)()
