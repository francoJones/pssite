(function () {
    let flag = document.querySelector('.flag')
    let clip = document.getElementById('SVGID_3_');
    flag.addEventListener('mouseover', onOver);
    flag.addEventListener('mouseout', onOut);

    function onOver() {
        clip.style.transform = 'scaleX(1)';
    }

    function onOut() {
        clip.style.transform = 'scaleX(0)';
    }

    clip.style.transform = 'scaleX(0)';
    setTimeout(function () {
        clip.style.transition = '.3s';
    })

})();
