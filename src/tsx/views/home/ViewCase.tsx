import * as React from 'react'
import * as css from '../../../scss/export.scss';

type props = {
    text?: string
}

export default class ViewCase extends React.PureComponent<props> {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        text: 'View'
    }

    render() {
        const text: string = this.props.text || ViewCase.defaultProps.text;
        // TODO figure out a better way to calculate svg text length. I guess the node width can be checked on DidMount
        const length: number = text.length > 4 ? 220 : 120; // 'view case' requires a way shorter background than 'View'

        return <svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 415 90' enableBackground='new 0 0 415 90'>

            <polygon id='flag' fill={css.red} points='0,52.34 77,90.34 77,39 0,0.34 '/>
            <g>
                <text transform='matrix(1 0 0 1 93.7598 77.8984)' fill={css.red}
                      className='flag__text'>{text}</text>
            </g>
            <clipPath id='SVGID'>
                <rect id='SVGID_3_' x='76' y='39' width='339' height='51.34' fill={css.red}/>
            </clipPath>
            <g className='clip' clipPath='url(#SVGID)'>
                <rect className='bg' x='76' y='39' width={length} height='51.34' fill={css.red}/>
                <text transform='matrix(1 0 0 1 93.7598 77.8984)' fill='#FFFFFF'
                      className='flag__text'>{text}</text>
            </g>

        </svg>

    }
}

