/** To keep things short, both model(nav) and reducer (RNav) are in the same file **/

export const Mnav = { // this is the 'fragment' argument in build###Model(fragment)
    hash: '',
    history: ['']
}

export function nav(state = Mnav, action) {
    if (typeof state === 'undefined') return null;

    let type = action.type
    let val = action.val;

    if (type === 'reset_all') return val.sections;
    if (type !== 'nav') return state;

    if (val.prop === 'hash') {
        let hash = val.val === undefined ? '' : val.val.toString();
        return Object.assign({}, state, {[val.prop]: hash.replace('#', '')});
    }

    if (val.prop === 'history') {
        let h = state.history;
        h.push(val.val)
        return Object.assign({}, state, {[val.prop]: h});
    }

    return Object.assign({}, state, {[val.prop]: val.val});
}
