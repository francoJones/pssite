<?php

const IS_DEV = false;

const JS_FILES = array(
    "dist/index.js",
);

const CSS_FILES = array(
    "dist/index.css"
);

include "php/Dev.php";

//-- NON-TEMPLATE edge does not support clip-path. This script is for the mail rollover effect
$EDGE_JS = (strpos(Dev::getBrowser(), "Edge") !== false) ? array("js/edge/mail.js") : null;
//--



?><!DOCTYPE html>
<html lang="en-UK" dir="ltr">
<head><title>. Paco Samino .</title>
    <base href="https://pacosamino.com/">
    <style>html {
            visibility: hidden;
        }</style>
    <?php

    include "html/meta_tags.html";
    Dev::inlineJS("js/hotjar.js");
    ?>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <?php


    //-- NON-TEMPLATE STUFF --//
    include "php/getFrontEndModel.php";
    //-----------------------//


    //-- MORE NON-TEMPLATE
    //$SHORT_URL = IS_DEV ? 'tstux' : Model::getShortUrl();
    $SHORT_URL = Model::getShortUrl();
    $HAS_THUMBS = $SHORT_URL ?? null;
    if (!$HAS_THUMBS) {
        if (IS_DEV)
            Dev::linkCSS("dist/indexNT.css");
        else
            Dev::inlineCSS("dist/indexNT.css");
    }

    //TODO do something about the default copy, it will need html tags
    // it should be Model::getCopy(), but it will be a string until the web is updated from 4.0 to 5.0
    $copy = Model::getCopy();//'user centered design<br>and development';


    $card = '
             <div class="card">							    
								<div class="name">PACO SAMINO</div>
								<div class="copy">' . $copy . '</div>
								<div class="mail">
								<div class="flag" style="transform: translateX(-93.7598px);">
								<a href="mailto:hey@pacosamino.com">
								<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 415 90" enable-background="new 0 0 415 90" xml:space="preserve">
	
                                <polygon id="flag" fill="#CB0916" points="0,50 64,82 64,32 0,0 "/>
                                <g>
                                    <text transform="matrix(1 0 0 1 93.7598 69)" fill="#CB0916"  class="flag__text">hey@pacosamino.com</text>
                                </g>
                                <clipPath id="SVGID">
                                <rect id="SVGID_3_" x="63" y="32" width="350" height="50" fill="#CB0916"/>
                                </clipPath>
                                    <g class="clip" clip-path="url(#SVGID)">
                                    <rect x="63" y="32" width="350" height="50" fill="#CB0916"/>
                                    <text transform="matrix(1 0 0 1 93.7598 69)" fill="#FFFFFF"  class="flag__text">hey@pacosamino.com</text>
                                   </g>
                               
                                </svg>
								</a>
								</div>
								</div> 
								
								
														
						</div>
             ';

    $logos = '
								<div  style="background-image:url(im/logos/samsung.svg)"    ><a target="_blank" href="http://www.design.samsung.com/global/contents/sde/"></a></div>
							    <div  style="background-image:url(im/logos/toyota.svg)"     ><a target="_blank" href="https://www.toyota-europe.com"></a></div>
							    <div  style="background-image:url(im/logos/ea.svg)"         ><a target="_blank" href="https://ea.com"></a></div>
							    <div  style="background-image:url(im/logos/fitbit.svg)"   ><a target="_blank" href="https://fitbit.com>"></a></div>
							    <div  style="background-image:url(im/logos/sage.svg)"       ><a target="_blank" href="https://sage.com"></a></div>
							    <div  style="background-image:url(im/logos/anaya.svg)"      ><a target="_blank" href="https://anaya.es"></a></div>
							    <div  style="background-image:url(im/logos/tonline.svg)"  ><a target="_blank" href="https://www.telekom.com/en/company"></a></div>
								';

    $linkedin = '<div id="linkedin' . ($HAS_THUMBS ? '' : '--nt') . '"><a href="http://www.linkedin.com/pub/paco-samino/44/11b/883" target="_blank">
<img src="im/linkedin.svg" /></a></div>';

    ?></head>
<body>
<?php
if ($HAS_THUMBS) echo '<div class="gallery"></div><div class="i-scroll">';
?>


<?php

if ($HAS_THUMBS) {
    echo '<div class="case"></div>';
    echo '<div class="home">';
    echo '<section class="splash">' . $card . '<div class="logos">
    <div class="logos__flex">' . $logos . '</div>
								</div>' . '</section>
<section id="app" data-short-url="' . $SHORT_URL . '" data-works-path="' . WORKS_PATH . '" ></section>
' . $linkedin . '</div>';
} else {


    echo '<main><section class="home">
						' . $card . '<div class="logos">' . $logos . '</div>' . $linkedin . '                       
                        
                    </section></main>
        
';
}
?>


<?php
if ($HAS_THUMBS) echo '</div>';// closes i-scroll
?>
<?php

if ($HAS_THUMBS) {

    if (IS_DEV)
        Dev::linkJS(JS_FILES);
    else
        Dev::inlineJS(JS_FILES);


    echo "
      <script>
(function(doc) {

    var addEvent = 'addEventListener',
        type = 'gesturestart',
        qsa = 'querySelectorAll',
        scales = [1, 1],
        meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];

    function fix() {
        meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
        doc.removeEventListener(type, fix, true);
    }

    if ((meta = meta[meta.length - 1]) && addEvent in doc) {
        fix();
        scales = [.25, 1.6];
        doc[addEvent](type, fix, true);
    }

}(document));
</script>
       
       

    	";
}//-- end has thumbs


if (IS_DEV)
    Dev::linkJS($EDGE_JS);
else
    Dev::inlineJS($EDGE_JS);


?>

</body>
<style>html {
        visibility: visible;
    }</style>
</html>


<?php

if (CACHE_ON) file_put_contents($__cachefile, ob_get_contents());
if (Dev::CREATE_DIST) file_put_contents(Dev::CREATE_DIST, ob_get_contents());
ob_end_flush();

?>
