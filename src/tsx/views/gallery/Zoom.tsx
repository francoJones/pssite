import * as React from 'react'
import Button from '@common/Button';

type props = {
    className?: string,
    onClick: () => void;
    bgColor: string;
    isZoomed: boolean;
}


export default class Zoom extends React.Component<props> {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        className: 'close'
    }

    onClick = () => {
        if (this.props.onClick) this.props.onClick()
    }

    render() {

        return <div className={this.props.className || Zoom.defaultProps.className} onClick={this.onClick}>

            <Button
                icon={
                    <React.Fragment>

                        <line fill='none' stroke='#FFFFFF' strokeWidth='4' strokeMiterlimit='10' x1='34' y1='52.631'
                              x2='52' y2='52.631'/>
                        {this.props.isZoomed ? null :
                            <line fill='none' stroke='#FFFFFF' strokeWidth='4' strokeMiterlimit='10' x1='43.5' y1='62'
                                  x2='43.5' y2='43'/>
                        }

                    </React.Fragment>
                }

                bgColor={this.props.bgColor}

            />
        </div>
    }
}
