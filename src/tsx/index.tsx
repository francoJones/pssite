import '../scss/ps.scss';

import * as React from 'react'
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {buildStore} from './store/Store';
import Home from '@views/home/Home';
import LetterFX from '@libs/LetterFX';

window.addEventListener('load',cardIntro)


// @ts-ignore
buildStore(document.getElementById('app').getAttribute('data-short-url'), (s) => { proceed(s) });
(document.querySelector('.splash') as HTMLElement).style.clipPath='inset(0 0 0 0)';

function proceed (store:any):void {

    ReactDOM.render(
        <Provider store={store}>
            <Home />
        </Provider>,
        document.getElementById('app')
    );

}


function cardIntro(){
    let letterFX:LetterFX= new LetterFX(['.name','.copy']);
    (document.querySelector('.card')! as HTMLElement).style.display='block';
    setTimeout(()=>letterFX.runIntro(()=>{
        (document.querySelector('.flag')! as HTMLElement).style.clipPath=`inset(0 0 0 0)`;
    }))
}

