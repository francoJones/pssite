'use-strict'
import * as React from "react"
import Play from './Play';
import {playBanner} from '@libs/Utils';
import {IImg} from '@T/types'
import {BANNERS, GALLERY, VID, IMG} from '@consts/DB';
import {LAZY, LAZYFX_LOADING} from '@consts/ClassNames'

/** for reference
 IImg{
    index?: number,
    src: string,
    onClick?: (p: number) => void,
    type: string | null,
    className?: string,
    showText:boolean,
    internal_link?:string
}*/

interface Iprops extends IImg {
    internal_link_type?: string,
    link?: string
    link_type?: string
    title?: string,
    text_above?: string,
    caption?: string,
    text_below?: string

}

export default class Img extends React.PureComponent<Iprops> { // TODO rename to Item, instead of Img

    contRef!: React.RefObject<HTMLDivElement>;
    videoContRef!: React.RefObject<HTMLDivElement>;
    btRef: any;

    static defaultProps = {
        className: 'pic',
        index: 0,
        showText: true
    }

    constructor(props) {
        super(props)
        this.contRef = React.createRef();
        this.videoContRef = React.createRef();
        this.btRef = React.createRef();
        this.onClick = this.onClick.bind(this);
    }


    onClick(e: React.MouseEvent<HTMLDivElement, MouseEvent | PointerEvent>): void {
        if (this.props.link) {
            window.open(this.props.link, this.props.link_type);
            return;
        }

        const type: string = this.props.internal_link_type || this.props.type || 'img'
        if (type === BANNERS) {
            this.playBanner(e, this.props.className!);
            return;
        }
        if (type === VID) {
            (this.btRef.current as Play).onClick(e)
            return;
        }
        if (this.props.onClick) this.props.onClick(this.props.index || Img.defaultProps.index)
    }

    playBanner(e: React.MouseEvent<HTMLDivElement, MouseEvent | PointerEvent>, className: string): void {
        let current: HTMLDivElement | null = (e.target as HTMLDivElement);

        // the img child is covered by an iframe by view(), which requires the parent container.
        // This while looks for said container.
        while (current) {
            current = (current.parentElement as HTMLDivElement);
            if (!current) continue;
            if (current.classList.value.indexOf(className) === -1) continue;

            break;
        }
        // If current ===null, means that what was clicked was not the content nor a preview button
        if (!current) return;

        playBanner(current, this.getPlayerContainer(), this.props.src);
    }

    getPlayerContainer = (): HTMLDivElement => {
        return this.videoContRef.current!;
    }


    getCaseImg(type: string | null, hasZoom: boolean, src: string, TODO_className?: string, caption?: string, internal_link?: string): JSX.Element | null {

        if (src === null) return null;
        const baseClass: string = 'item__img';
        return <div className={baseClass} onClick={this.onClick}>
            <div className='item__video-cont' ref={this.videoContRef}/>
            <picture className={baseClass + (type === VID ? '-vid-poster' : '-pic')}>
                <img className={LAZY} data-src={src} src={'im/placeholder.gif'}/>
            </picture>
            {caption ? <div className={baseClass + '-caption'}>{caption}</div> : null}
            {(type === VID || type === BANNERS) ?
                <Play internalLink={internal_link!} getPlayerContainer={this.getPlayerContainer}
                      behavior={type === VID ? 'CASE' : BANNERS} className={baseClass + '-bt player'}
                      type={type} ref={this.btRef}/>
                : null
            }
        </div>
    }


    getGalleryImg = (src: string, type: string | null, TODO_className: string): JSX.Element => {

        if (type === VID) {
            return <div className='vid__flex'>
                <Play internalLink={this.props.internal_link!} getPlayerContainer={this.getPlayerContainer}
                      behavior={GALLERY}
                      className={'vid__poster-play-bt'} type={type} ref={this.btRef}/>
                <div className='vid__player-cont' ref={this.videoContRef}/>
                <div className='vid__poster-cont'>
                    <picture className={'vid__poster'}><img src={src}/></picture>
                </div>
            </div>
        } else
            return <React.Fragment>

                <picture className={`pic ${this.props.isZoomed ? ' pic--zoomed' : ''}`}><img src={src}/></picture>
            </React.Fragment>
    }


    render() {
        const {className, src, type, title, text_above, caption, text_below, internal_link, internal_link_type, link, showText}: Iprops = this.props;


        /** FOR CONSIDERATION
         * the preview button of  full row images, has been removed, as they are large enough and show smaller in Preview
         * ATM I don't know how good an idea that is
         */
        const hasZoom: boolean = (internal_link_type || link) ? true : false;
        /** -------------------------------------------------------------------------------------------------------**/
        const baseClass: string = className || Img.defaultProps.className;

        if (!showText)
            return this.getGalleryImg(src, type, baseClass)
        else

            return <div className={`${className} ${LAZYFX_LOADING}`} ref={this.contRef}>
                {title ? <div className='item__title'>{title}</div> : null}
                <div className='item__flex'>
                    {text_above ? <div className={'item__text-above'}>
                        <div dangerouslySetInnerHTML={{__html: text_above}}/>
                    </div> : null}
                    {this.getCaseImg(type, hasZoom, src, baseClass, caption, internal_link)}
                    {text_below ?
                        <div className={`item__text-below`} dangerouslySetInnerHTML={{__html: text_below}}/> : null}
                </div>
            </div>

    }
}
