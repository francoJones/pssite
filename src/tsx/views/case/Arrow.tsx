import * as React from 'react'
import Button from '@common/Button';

type action = 'BACK' | 'TOP'

interface IProps {
    onClick: () => void;
    className?: string;
    type?: action
}

export default class Arrow extends React.Component<IProps> {

    nodeRef!: React.RefObject<HTMLDivElement>;

    constructor(props) {
        super(props);
        this.nodeRef = React.createRef();
        this.onClick = this.onClick.bind(this);
    }


    static defaultProps = {
        className: 'arrow_back',
        type: 'BACK'
    }

    onClick(): void {

        if (this.props.type === 'TOP')
            window.scroll(0, 0);
        else
            this.props.onClick()

    }

    startOutro = (): void => {
        const style: { [k: string]: any } = this.nodeRef.current!.style;
        style.pointerEvents = 'none';
        style.transform = 'scale(0)';
    }


    render() {
        // the ${77/2},${91/2} comes from the viewBox width,height of <Button>
        return <div className={this.props.className || Arrow.defaultProps.className} onClick={this.onClick}
                    ref={this.nodeRef}>

            <Button icon={<g
                transform={this.props.type === 'TOP' ? `rotate(90,${83 / 2},${100.469 / 2}) translate(0,-5)` : ''}>
                <polyline fill='none' stroke='#FFFFFF' strokeWidth='2.8' strokeMiterlimit='10' points='42.49,43.328 32.277,53.542
		42.311,63.576 	'/>
                <polyline fill='none' stroke='#FFFFFF' strokeWidth='2.8' strokeMiterlimit='10' points='54.723,43.328 44.51,53.542
		54.544,63.576 	'/>
            </g>
            }/>

        </div>
    }
}

